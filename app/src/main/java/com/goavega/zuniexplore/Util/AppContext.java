package com.goavega.zuniexplore.Util;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import com.goavega.zuniexplore.Model.Answer;
import com.goavega.zuniexplore.Model.User;
import java.util.ArrayList;

public class AppContext  extends Application {

    private AppContext instance;
    private PowerManager.WakeLock wakeLock;
    private OnScreenOffReceiver onScreenOffReceiver;
    
    private User user;
    public ArrayList<Answer> answerList;
    public ArrayList<String> categoryList;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        user = new User();
        answerList = new ArrayList<Answer>();
        categoryList = new ArrayList<String>();
        
        categoryList.add("White Flame");
        categoryList.add("Yellow Flame");
        categoryList.add("Orange Flame");
        categoryList.add("Blue Flame");
        
        registerKioskModeScreenOffReceiver();
        startKioskService();

    }

    private void registerKioskModeScreenOffReceiver() {
        // register screen off receiver
        final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        onScreenOffReceiver = new OnScreenOffReceiver();
        registerReceiver(onScreenOffReceiver, filter);
    }

    public PowerManager.WakeLock getWakeLock() {
        if(wakeLock == null) {
            // lazy loading: first call, create wakeLock via PowerManager.
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wakeup");
        }
        return wakeLock;
    }

    private void startKioskService() { // ... and this method
        startService(new Intent(this, KioskService.class));
    }
    
    public ArrayList<Answer> getAnswerListObject(){
    	return answerList;
    }
    
    public void setAnswerListObject(){
    	 answerList = new ArrayList<Answer>();
    }
    
    public ArrayList<String> getCategoryListObject(){
    	return categoryList;
    }
    
    public User getUserObject(){
    	return user;
    }
}