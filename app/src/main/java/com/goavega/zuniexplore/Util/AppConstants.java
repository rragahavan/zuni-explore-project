package com.goavega.zuniexplore.Util;

public class AppConstants {

	public static final String APPLICATION_PACKAGE_NAME = "com.goavega.zuniexplore";
	
	public static final String SERVER_END_POINT = "http://gvg-u1.cloudapp.net:8080/api/";
	
	public static final String POST_USER_API = "users" ;
	
	public static final String POST_USER_ANSWERS_API = "responses" ;
	
	public static String CONNECTING = "In Progress...";
	
	public static final String SERVER_ERROR = "{\"success\":\"false\"}";
	
}
