package com.goavega.zuniexplore.Model;

/**
 * Created by Administrator on 08-11-2016.
 */
public class Pricing {

    private String Price;

    private String Unit;

    public String getPrice ()
    {
        return Price;
    }

    public void setPrice (String Price)
    {
        this.Price = Price;
    }

    public String getUnit ()
    {
        return Unit;
    }

    public void setUnit (String Unit)
    {
        this.Unit = Unit;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Price = "+Price+", Unit = "+Unit+"]";
    }
}
