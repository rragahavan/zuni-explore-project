package com.goavega.zuniexplore.Model;

/**
 * Created by Administrator on 08-11-2016.
 */
public class ItemModel {

    private Items[] items;

    private String type;

    private String key;

    public Items[] getItems ()
    {
        return items;
    }

    public void setItems (Items[] items)
    {
        this.items = items;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [items = "+items+", type = "+type+", key = "+key+"]";
    }
}
