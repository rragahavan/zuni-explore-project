package com.goavega.zuniexplore.View;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.goavega.zuniexplore.Adapters.ItemNameAdapter;
import com.goavega.zuniexplore.Model.ItemModel;
import com.goavega.zuniexplore.Network.ConnectionDetector;
import com.goavega.zuniexplore.Network.ItemServiceApi;
import com.goavega.zuniexplore.R;
import java.util.ArrayList;

public class ItemNameActivity extends AppCompatActivity {

    ImageView searchIcon;
    ArrayList<ItemModel> itemList=new ArrayList<ItemModel>();
    ArrayList<String> itemNameList=new ArrayList<String>();
    RecyclerView mRecyclerView;
    ItemNameAdapter adapter;
    EditText searchItemText;
    String searchItem;

    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_names);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        searchItemText=(EditText)findViewById(R.id.searchItemText);

        connectionDetector=new ConnectionDetector(ItemNameActivity.this);

        if(connectionDetector.isNetworkAvailable()) {
            final ProgressDialog dialog = ProgressDialog.show(ItemNameActivity.this, "",
                    "Please wait...", true);
            dialog.show();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            }, 6000);

            itemList = new ItemServiceApi(ItemNameActivity.this).getAllItemDetails();
        }else
            Toast.makeText(ItemNameActivity.this,"please check your internet connection",Toast.LENGTH_LONG).show();


        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchIcon=(ImageView)findViewById(R.id.searchIcon);
        for(int k=0;k<itemList.size();k++)
        {
            itemNameList.add(itemList.get(k).getType());
        }
        adapter = new ItemNameAdapter(itemNameList);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final ProgressDialog dialog = ProgressDialog.show(ItemNameActivity.this, "",
                        "Please wait...", true);
                dialog.show();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 6000);
                Intent i = new Intent(ItemNameActivity.this, ItemDetailsActivity.class);
                i.putExtra("itemType", itemNameList.get(position));
                startActivity(i);

            }
        }));



        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(connectionDetector.isNetworkAvailable()) {

                    final ProgressDialog dialog = ProgressDialog.show(ItemNameActivity.this, "",
                            "Please wait...", true);
                    dialog.show();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    }, 6000);


                    searchItem = searchItemText.getText().toString();

                    if (!searchItem.equals("")) {
                        Intent i = new Intent(ItemNameActivity.this, SearchItemActivity.class);
                        i.putExtra("searchItemValue", searchItem);
                        startActivity(i);
                    } else {
                        Toast.makeText(getApplicationContext(), "Enter Search Item value", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                 Toast.makeText(getApplicationContext(),"Please check your internet connection",Toast.LENGTH_LONG).show();
                }
             }
        });

    }

    private interface ClickListener {
        void onClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        final private GestureDetector gestureDetector;
        final private ItemNameActivity.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ItemNameActivity.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

}

