package com.goavega.zuniexplore.View;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

//import com.goavega.zuniexplore.Util.AppContext;
import com.goavega.zuniexplore.R;
import com.goavega.zuniexplore.Model.Answer;
import com.goavega.zuniexplore.Util.AppContext;
import com.goavega.zuniexplore.Util.AppUtil;
import com.goavega.zuniexplore.Model.Product;

import java.util.ArrayList;

public class Question1 extends Activity {

	private Button option_btn_1,option_btn_2,option_btn_3,option_btn_4;
	private TextView pageNoTextView,restartTv;
	private ImageView restart_Img;
	
	private AppContext appContext;
	private Answer answerObject;
	private Product product1,product2,product3,product4;
	private ArrayList<Product> productList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question1_layout);
		
		appContext = (AppContext)getApplication();
		
		pageNoTextView = (TextView)findViewById(R.id.pageno_tv);
		pageNoTextView.setText("1/6");
		
		/*restartTv = (TextView)findViewById(R.id.restart_tv);
		restartTv.setOnClickListener(onOptionButtonClickListener);
		
		restart_Img = (ImageView)findViewById(R.id.back);
		restart_Img.setOnClickListener(onOptionButtonClickListener);*/
		
		option_btn_1 = (Button)findViewById(R.id.option1_btn);
		option_btn_2 = (Button)findViewById(R.id.option2_btn);
		option_btn_3 = (Button)findViewById(R.id.option3_btn);
		option_btn_4 = (Button)findViewById(R.id.option4_btn);
		
		option_btn_1.setOnClickListener(onOptionButtonClickListener);
		option_btn_2.setOnClickListener(onOptionButtonClickListener);
		option_btn_3.setOnClickListener(onOptionButtonClickListener);
		option_btn_4.setOnClickListener(onOptionButtonClickListener);
		
		answerObject = new Answer();
		answerObject.setQuestionNo("1");
		
		productList = new ArrayList<Product>();
		
	}
	
	public OnClickListener onOptionButtonClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			switch(v.getId()){
				case R.id.option1_btn:
																		
										answerObject.setOptionNo("1");
										
										product1 = new Product();
										product1.setName(appContext.getCategoryListObject().get(2));  // Orange Flame
										product1.setValue(1);
										
										productList.add(product1);
										
										answerObject.setProductList(productList);
										
										AppUtil.navigateToScreen(Question1.this, "com.goavega.zuniexplore.View.Question2");
										
										break;
				case R.id.option2_btn:
										answerObject.setOptionNo("2");
										
										product1 = new Product();
										product1.setName(appContext.getCategoryListObject().get(1));   // Yellow Flame
										product1.setValue(1);
										
										productList.add(product1);
										
										answerObject.setProductList(productList);
										
										AppUtil.navigateToScreen(Question1.this, "com.goavega.zuniexplore.View.Question2");
										break;
				case R.id.option3_btn:
										answerObject.setOptionNo("3");
										
										product1 = new Product();
										product1.setName(appContext.getCategoryListObject().get(0));   // White Flame
										product1.setValue(1);
										
										product2 = new Product();
										product2.setName(appContext.getCategoryListObject().get(3));   // Blue Flame
										product2.setValue(2);
										
										productList.add(product1);
										productList.add(product2);
										
										answerObject.setProductList(productList);
										
										AppUtil.navigateToScreen(Question1.this, "com.goavega.zuniexplore.View.Question2");
										break;
				case R.id.option4_btn:
										answerObject.setOptionNo("4");
										
										product1 = new Product();
										product1.setName(appContext.getCategoryListObject().get(0));   // White Flame
										product1.setValue(2);
										
										product2 = new Product();
										product2.setName(appContext.getCategoryListObject().get(3));   // Blue Flame
										product2.setValue(1);
										
										productList.add(product1);
										productList.add(product2);
										
										answerObject.setProductList(productList);
										
										AppUtil.navigateToScreen(Question1.this, "com.goavega.zuniexplore.View.Question2");
										break;

				case R.id.restart_tv: 
										appContext.setAnswerListObject();
										//AppUtil.navigateToScreen(Question1.this, "com.android.wildfire.MainActivity");
										//AppUtil.navigateToHomeScreen(Question1.this)
					                    //AppUtil.navigateToScreen(Question1.this, "com.android.wildfire.HomeActivity");
					                    AppUtil.navigateToScreen(Question1.this, "com.goavega.zuniexplore.View.HomeActivity");
									    break;
									   
				case R.id.back: 
										appContext.setAnswerListObject();
//										AppUtil.navigateToScreen(Question1.this, "com.android.wildfire.MainActivity");
										AppUtil.navigateToHomeScreen(Question1.this);
									    break;
				default:
						AppUtil.navigateToScreen(Question1.this, "com.goavega.zuniexplore.View.Question2");
						break;

			}
			
			// Add answer to the list
			appContext.getAnswerListObject().add(answerObject);
			
		}
	};
	
	
	
}
