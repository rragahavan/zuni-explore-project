package com.goavega.zuniexplore.View;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.goavega.zuniexplore.Util.AppConstants;
import com.goavega.zuniexplore.Util.AppContext;
import com.goavega.zuniexplore.View.MainActivity;

import com.goavega.zuniexplore.R;

//import com.android.wildfire.network.HttpAsyncTask;
import com.goavega.zuniexplore.Network.ServiceListener;
import com.goavega.zuniexplore.Util.AppUtil;



import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends Activity implements ServiceListener{
	
	private EditText nameEdtTxt, emailEdtTxt;
	private Button submitBtn;
	private AppContext appContext;
	private TextView restartTv;
	private String name, email ;
	private ImageView restart_Img;
	private JSONObject userObject;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.email_screen);
		
		appContext = (AppContext)getApplication();
		
		nameEdtTxt = (EditText)findViewById(R.id.name_edttxt);
		emailEdtTxt = (EditText)findViewById(R.id.email_edttxt);
		
		submitBtn = (Button)findViewById(R.id.submit_btn);
		submitBtn.setOnClickListener(onSubmitClickListener);
		
		restartTv = (TextView)findViewById(R.id.restart_tv);
		restartTv.setOnClickListener(onRestartClickListener);
		
		/*restart_Img = (ImageView)findViewById(R.id.back);
		restart_Img.setOnClickListener(onRestartClickListener);*/
		
		emailEdtTxt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
				
			}
		});
		
		emailEdtTxt.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch(event.getAction()) {
	            case MotionEvent.ACTION_DOWN:
	            	InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
	                return true;  
				}
				return false;
			}
		});
		
		emailEdtTxt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		            if (!hasFocus) {
		                hideKeyboard(v);
		            }
		        }
		    });
		
				
	}
	
	protected OnClickListener onSubmitClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			
			name = nameEdtTxt.getText().toString().trim();
			email = emailEdtTxt.getText().toString().trim();
			
			appContext.getUserObject().setName(name);
			appContext.getUserObject().setEmailId(email);
			
			try {
				
				userObject = new JSONObject();
				
				if(name!=null){
					userObject.put("name", name);
				}else{
					userObject.put("name", "");
				}
				
				if(null != email){
				
					if(!email.equals("") || email.length()!=0){
						userObject.put("email", email);
					}else{
						userObject.put("email", "");
					}	
				}
				
				System.out.println("USER : "+ userObject);
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			// post to server
			

			/*HttpAsyncTask httpAsyncTask = new HttpAsyncTask(LoginActivity.this, userObject, LoginActivity.this);
			httpAsyncTask.execute(AppConstants.SERVER_END_POINT + AppConstants.POST_USER_API);*/


			Intent nxtIntent = new Intent(LoginActivity.this,Result.class);
			startActivity(nxtIntent);
		}
		
		
	};
	
	protected OnClickListener onRestartClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			appContext.setAnswerListObject();
			Intent nxtIntent = new Intent(LoginActivity.this,HomeActivity.class);
			startActivity(nxtIntent);
			finish();
		}
		
		
	};

	@Override
	public void onServiceComplete(String posts) {

		boolean serverResponse = AppUtil.ParseJson(posts);
		
		String respString = (serverResponse)? "Success" : "Failure";
		
		System.out.println("Server Response : "+ respString);
		
	}
	
	  public void hideKeyboard(View view) {
	        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	    }
	
	    protected void showAlert(){

	    	AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
	    	alertDialog.setTitle("Alert");
	    	alertDialog.setMessage("Please enter valid email address");
	    	alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
	    	    new DialogInterface.OnClickListener() {
	    	        public void onClick(DialogInterface dialog, int which) {
	    	            dialog.dismiss();
	    	        }
	    	    });
	    	alertDialog.show();
		}

}
