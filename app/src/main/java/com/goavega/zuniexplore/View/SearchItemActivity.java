package com.goavega.zuniexplore.View;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.goavega.zuniexplore.Adapters.SearchItemAdapter;
import com.goavega.zuniexplore.Model.Items;
import com.goavega.zuniexplore.Network.ConnectionDetector;
import com.goavega.zuniexplore.Network.ItemServiceApi;
import com.goavega.zuniexplore.R;
import java.util.ArrayList;

public class SearchItemActivity extends AppCompatActivity {
    TextView typeText;
    RecyclerView mRecyclerView;
    SearchItemAdapter itemDetailsAdapter;
    ArrayList<Items> filterItems=new ArrayList<Items>();
    ConnectionDetector connectionDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_item);
        connectionDetector=new ConnectionDetector(SearchItemActivity.this);


        typeText=(TextView) findViewById(R.id.typText);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(connectionDetector.isNetworkAvailable()) {

            final ProgressDialog dialog = ProgressDialog.show(SearchItemActivity.this, "",
                    "Please wait...", true);
            dialog.show();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            }, 4000);


            String searchItemValue = getIntent().getStringExtra("searchItemValue");
            typeText.setText(getString(R.string.search_result) + " " + "\"" + searchItemValue + "\"");
            filterItems = new ItemServiceApi(SearchItemActivity.this).filterItems(searchItemValue);
            itemDetailsAdapter = new SearchItemAdapter(filterItems);
            mRecyclerView.setAdapter(itemDetailsAdapter);
        }
        else
        {
            Toast.makeText(SearchItemActivity.this,"Please check your internet connection",Toast.LENGTH_LONG).show();
        }
    }
}
