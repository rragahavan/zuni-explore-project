package com.goavega.zuniexplore.View;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.goavega.zuniexplore.Util.AppContext;
import com.goavega.zuniexplore.R;
import com.goavega.zuniexplore.Model.Answer;
import com.goavega.zuniexplore.Util.AppUtil;
import com.goavega.zuniexplore.Model.Product;

import java.util.ArrayList;

public class Question3 extends Activity {

	private Button option_btn_1,option_btn_2,option_btn_3,option_btn_4;
	private TextView pageNoTextView,restartTv;
	private ImageView restart_Img;
	private AppContext appContext;
	private Answer answerObject;
	private Product product1,product2,product3,product4;
	private ArrayList<Product> productList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question3_layout);
		
		appContext = (AppContext)getApplication();
		
		pageNoTextView = (TextView)findViewById(R.id.pageno_tv);
		pageNoTextView.setText("3/6");

		/*restartTv = (TextView)findViewById(R.id.restart_tv);
		restartTv.setOnClickListener(onOptionButtonClickListener);
		
		restart_Img = (ImageView)findViewById(R.id.back);
		restart_Img.setOnClickListener(onOptionButtonClickListener);*/
		
		option_btn_1 = (Button)findViewById(R.id.option1_btn);
		option_btn_2 = (Button)findViewById(R.id.option2_btn);
		option_btn_3 = (Button)findViewById(R.id.option3_btn);
		option_btn_4 = (Button)findViewById(R.id.option4_btn);
		
		option_btn_1.setOnClickListener(onOptionButtonClickListener);
		option_btn_2.setOnClickListener(onOptionButtonClickListener);
		option_btn_3.setOnClickListener(onOptionButtonClickListener);
		option_btn_4.setOnClickListener(onOptionButtonClickListener);
		
		answerObject = new Answer();
		answerObject.setQuestionNo("3");
		
	}
	
	public OnClickListener onOptionButtonClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			switch(v.getId()){
				case R.id.option1_btn:
										answerObject.setOptionNo("1");
										AppUtil.navigateToScreen(Question3.this, "com.goavega.zuniexplore.View.Question4");
										
										break;
				case R.id.option2_btn:
										answerObject.setOptionNo("2");		
										AppUtil.navigateToScreen(Question3.this, "com.goavega.zuniexplore.View.Question4");
										break;
				case R.id.option3_btn:
										answerObject.setOptionNo("3");
										AppUtil.navigateToScreen(Question3.this, "com.goavega.zuniexplore.View.Question4");
										break;
				case R.id.option4_btn:
										answerObject.setOptionNo("4");
										AppUtil.navigateToScreen(Question3.this, "com.goavega.zuniexplore.View.Question4");
										break;
				case R.id.restart_tv: 
										appContext.setAnswerListObject();
//										AppUtil.navigateToScreen(Question3.this, "com.android.wildfire.MainActivity");
										AppUtil.navigateToHomeScreen(Question3.this);
										break;
				case R.id.back: 
										appContext.setAnswerListObject();	
//										AppUtil.navigateToScreen(Question3.this, "com.android.wildfire.MainActivity");
										AppUtil.navigateToHomeScreen(Question3.this);
										break;
				default:
						AppUtil.navigateToScreen(Question3.this, "com.goavega.zuniexplore.View.Question4");
						break;

			}
			
			//
			appContext.getAnswerListObject().add(answerObject);

		}
	};
	
	
	
}
